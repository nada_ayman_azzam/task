import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { Products } from '../../models/products';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    products: Products[] = []
  // = [
  //   {name: 'photo1', price: 20 , photoUrl: 'assets/download.jpg'},
  //   {name: 'photo2', price: 20 , photoUrl: 'assets/download (1).jpg'},
  //   {name: 'photo3', price: 20 , photoUrl: 'assets/download (2).jpg'}
  // ]

  constructor( private productsService : ProductsService) { }

  ngOnInit(): void {
    this.productsService.getProduct();
  }

  getProduct() {
    this.productsService.getProduct().subscribe(
      // the first argument is a function which runs on success
      (data : any[]) => { this.products = data},
      // the second argument is a function which runs on error
      err => console.error(err),
      // the third argument is a function which runs on completion
      () => console.log('done loading products')
    );
  }
}
